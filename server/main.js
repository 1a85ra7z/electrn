const log = require('./modules/log');

const auth = require('./modules/authentication');
const init = require('./modules/init').f;
const settings = require('./modules/settings');
const publicPosts = require('./modules/publicPosts');

let encodeHTML = (t) => {
    let m = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return t.replace(/[&<>"']/g, (r) => { return m[r]; });
};

init((socket, database) => {
    socket.on('bug', (o) => {
        database.collection("bugs").insertOne({
            m: o.m,
            time: new Date(),
            fixed: false
        })
    });

    auth(socket, database, (user) => {
        socket.on('disconnect', () => {
            database.collection('settings').updateOne({
                token: user.token
            }, {
                $set: {
                    online: false
                }
            });
        });

        settings(socket, database, user);
        publicPosts(socket, database, user);

        socket.on("token-via-aid", (aid) => {
            database.collection("authentication").find({
                aid: aid
            }).toArray((err, users) => {
                socket.emit("token-via-aid-response", users[0].token, users[0].u);
            });
        });

        socket.on("get-chats", () => {
            database.collection("chats").find({
                u: { $in: [user.token]},
                visible: true
            }).sort({time: 1}).toArray((err, chats) => {
                let final = [];

                chats.forEach((chat, i) => {
                    delete chat["_id"];
                    chat.unread = 0;

                    database.collection("messages").find({
                        u: {
                            $all: [chat.u[0], chat.u[1]]
                        },

                        visible: {
                            $in: [user.token]
                        }
                    }).toArray((err, messages) => {
                        if(messages.length > 0) {
                            messages.forEach((message) => {
                                if(message.status !== 2) {
                                    chat.unread++;
                                }
                            });

                            chat.lastMessage = messages[messages.length - 1];
                        } else {
                            chat.lastMessage = {
                                text: "<i>No messages.</i>",
                                time: null
                            }
                        }

                        final.push(chat);

                        if(i === chats.length - 1 && final.length === chats.length) {
                            socket.emit("get-chats-result", final.sort((a, b) => {
                                return new Date(b.time) - new Date(a.time);
                            }));
                        }
                    });
                });

                if(chats.length === 0) {
                    socket.emit("get-chats-result", []);
                }
            });
        });

        socket.on('delete-message', (o) => {
            if(o.mode === 0) {
                database.collection("messages").updateOne(
                    {
                        $or: [{
                            u: [
                                user.token, o.token
                            ],
                        }, {
                            u: [
                                o.token, user.token
                            ],
                        }],


                        id: o.id
                    },

                    {
                        $pull: {
                            visible: user.token
                        }
                    }
                );
            } else if(o.mode === 1) {
                database.collection("messages").updateOne(
                    {
                        u: [
                            user.token, o.token
                        ],

                        id: o.id
                    },

                    {
                        $pull: {
                            visible: {
                                $in: [
                                    user.token, o.token
                                ]
                            }
                        }
                    }
                );
            }
        });

        socket.on('get-messages', (o) => {
            if(typeof o === "object") {
                if(typeof o.count === "number" && typeof o.token === "string") {
                    o.token = encodeHTML(o.token);

                    database.collection("messages").find({
                        u: {
                            $all: [user.token, o.token]
                        },

                        visible: {
                            $in: [user.token]
                        }
                    }).sort({
                        _id: -1
                    }).limit(o.count).toArray((err, messages) => {
                        if(messages.length > 0) {
                            socket.emit(`get-messages-result-${o.token}`, messages.length);

                            messages.forEach((message, index) => {
                                delete message["_id"];

                                socket.emit(`get-messages-result-${o.token}-${index}`, message);

                                database.collection('messages').update({
                                    id: message.id,
                                    u: [o.token, user.token],
                                    seen: false
                                }, {
                                    $set: {
                                        seen: new Date(),
                                        status: 2
                                    }
                                });
                            });
                        } else {
                            socket.emit(`get-messages-result-${o.token}`, 0);
                        }
                    })
                }
            }
        });

        socket.on('new-message', (o) => {
            let timeStamp = new Date();

            database.collection("messages").insertOne({
                u: [user.token, encodeHTML(o.token)],
                sentBy: user.token,
                text: encodeHTML(o.text),
                time: timeStamp,
                status: 1,
                seen: false,
                type: encodeHTML(o.type),
                visible: [user.token, encodeHTML(o.token)],
                id: (Math.random().toString(36).substr(2, 9) +
                    Math.random().toString(36).substr(2, 9) +
                    Math.random().toString(36).substr(2, 9) +
                    Math.random().toString(36).substr(2, 9) +
                    Math.random().toString(36).substr(2, 9) +
                    Math.random().toString(36).substr(2, 9) +
                    Math.random().toString(36).substr(2, 9) +
                    Math.random().toString(36).substr(2, 9)).toUpperCase(),
                options: o.options
            });

            database.collection("chats").updateOne({
                'chatId': encodeHTML(o.chatId)
            }, { $set: {visible: true}});
        });

        socket.on('search', (t) => {
           database.collection('chats').find({
               u: [user.token, t]
           }).toArray((err, chats) => {
               if (chats.length === 0) {
                   database.collection('authentication').find({
                       'token': t
                   }).toArray((error, usr) => {
                       let chatId = '_' + (Math.random().toString(36).substr(2, 9) +
                           Math.random().toString(36).substr(2, 9) +
                           Math.random().toString(36).substr(2, 9) +
                           Math.random().toString(36).substr(2, 9)).toUpperCase();

                       database.collection('chats').insertOne({
                           'u': [user.token, t],
                           'clear': [user.u, usr[0].u],
                           'chatId': chatId,
                           'visible': false,
                           'time': new Date()
                       });

                       socket.emit('search-opened', chatId);
                   });
               } else {
                   socket.emit('search-opened', chats[0].chatId)
               }
           });
        });

        socket.on('search-check-name', (n) => {
            if(n !== user.u) {
                let res = [];

                database.collection("authentication").find({
                    'u': {$regex : ".*" + n + ".*"}
                }).toArray((err, na) => {
                    if(na.length > 0) {
                        [...na].forEach((da) => {
                            if(da.u !== user.u) {
                                res.push({
                                    'name': da.u,
                                    'token': da.token
                                });
                            }
                        });

                        socket.emit('search-check-name-result', res);
                    } else {
                        socket.emit('search-check-name-no-results');
                    }
                });
            } else {
                socket.emit('search-check-name-own-name');
            }
        })
    });
});