const chalk = require('chalk');

module.exports = (d) => {
    console.log(chalk.grey(`[${chalk.yellow('LOG')}] ${chalk.white(d)}`))
};