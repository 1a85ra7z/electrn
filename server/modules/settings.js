const fs = require('fs');
const base64Img = require('base64-img');
const sharp = require('sharp');

module.exports = (socket, database, user) => {
    socket.on('check-aids-online-state', (d) => {
        database.collection("settings").find({
            'token': d
        }).toArray((e, data) => {
            socket.emit('check-aids-online-state-result', data[0].online);
        })
    });

    socket.on("get-aids-picture", (token) => {
        database.collection("settings").find({
            'token': token
        }).toArray((e, d) => {
            if((d.length > 0 && d[0].pictureVisible) || token === user.token) {
                fs.readdir(`data/users/${token}/profile-pictures/`, function(e, f) {
                    if (f.length === 0) {
                        socket.emit('aids-picture', null);
                    } else {
                        base64Img.base64(`data/users/${token}/profile-pictures/${f[f.length - 1]}`, (e, data) => {
                            socket.emit(`user-picture-${token}`, data);
                        });
                    }
                });
            }
        });
    });

    socket.on("new-profile-picture", (d) => {
        fs.readdir(`data/users/${user.token}/profile-pictures/`, function(e, f) {
            let b = 0;

            if (f !== undefined) {
                b = f.length + 1;
            }

            let buffer = Buffer.from(d.data.replace(/^data:image\/[a-zA-Z]+;base64,/, ''), 'base64');

            sharp(buffer).metadata()
                .then((m) => {
                    let max;
                    let left = 0;
                    let top = 0;

                    if(m.width > m.height) {
                        max = m.height;
                        left = Math.floor((m.width - m.height) / 2);
                    } else if(m.width < m.height) {
                        max = m.width;
                        top = Math.floor((m.height - m.width) / 2);
                    }

                    return sharp(buffer)
                        .extract({ width: max, height: max, left: left, top: top })
                        .toBuffer();
                })
                .then(function(data) {
                    fs.writeFile(`data/users/${user.token}/profile-pictures/${b}.${d.extension.toLowerCase().replace('image/', '')}`, data, () => {
                        socket.emit('set-new-profile-picture');
                    });
                });
        });
    });
};