const app = require('express')();
const http = require('http').Server(app);
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const chalk = require('chalk');
const log = require('./log');
const io = require('socket.io')(http);

let client = new MongoClient(url);
let database;
let connections = 0;

f = (c, d) => {
    client.connect(() => {
        database = client.db('electrn');

        log(`Connected successfully to ${chalk.yellow('MongoDB')}`);

        http.listen(8810, function () {
            log(`Listening on ${chalk.yellow('localhost' + chalk.grey(':') + chalk.red('8810'))}`);

            io.on('connection', (socket) => {
                connections++;
                log(chalk.green.bold("CONNECTION " + chalk.grey("[" + chalk.yellow(connections) + "]")));

                socket.on('disconnect', () => {
                    connections--;
                    log(chalk.red.bold("DISCONNECTED " + chalk.grey("[" + chalk.yellow(connections) + "]")))
                });

                c(socket, database);
            });
        });
    }, {
        useNewUrlParser: true,
        server: {socketOptions: {keepAlive: 1, connectTimeoutMS: 30000}},
        replset: {socketOptions: {keepAlive: 1, connectTimeoutMS: 30000}}
    });
};

app.use(require('express').static('../client'));
app.use('/beta', require('express').static('../beta'));
app.use('/user', require('express').static('./data/aids'));

app.listen(3465, () => {});


module.exports = {
    s: io,
    f: f
};
