const fs = require('fs');

const chalk = require('chalk');
const log = require('./log');

const { createCanvas } = require('canvas');

let colors = ['#FF9F1C', '#E71D36', '#2EC4B6', '#011627'];

module.exports = (socket, database, c) => {
    let user;

    let performLogin = () => {
        socket.on("create-login-auth-code", () => {
            log("Requested auth-code");

            let authCode = (Math.random().toString(36).substr(2, 9) +
                Math.random().toString(36).substr(2, 9) +
                Math.random().toString(36).substr(2, 9) +
                Math.random().toString(36).substr(2, 9)).toUpperCase();

            database.collection("authentication").updateOne(
                {'u': user.u, 'p': user.p},
                { $set: {
                        'a': authCode
                    }
                }
            );

            socket.emit("new-auth-code",  authCode);
        });

        database.collection('settings').updateOne({
            token: user.token
        }, {
            $set: {
                online: true
            }
        });

        socket.emit("logged-in", user);
        log(socket.request.connection.remoteAddress + chalk.yellow(' LOGGED IN'))
        c(user);
    };

    socket.on('check-auth-code', (d) => {
        database.collection('authentication').find({
            'a': d
        }).toArray((e, r) => {
            if (r.length > 0) {
                user = r[0];
                performLogin();
            } else {
                socket.emit("failed-log-in");
            }
        });
    });

    socket.on('check-aids-data', (d) => {
        database.collection('authentication').find({
            'u': d.u, 'p': d.p
        }).toArray((e, r) => {
            if (r.length > 0) {
                user = r[0];

                performLogin();
            } else {
                database.collection('authentication').find({
                    'u': d.u
                }).toArray((er, re) => {
                    if (re.length > 0) {
                        socket.emit("name-in-use");
                    } else {
                        socket.emit("name-not-in-use");
                    }
                });
            }
        })
    });

    socket.on('sign-up', (d) => {
        database.collection('authentication').find({
            'u': d.u
        }).toArray((e, r) => {
            if (!r.length > 0) {
                let aid = '@' + Math.floor(Math.random() * (99999999 - 10000000 + 1) + 10000000).toString();

                let token = '_' +
                    (Math.random().toString(36).substr(2, 9) +
                    Math.random().toString(36).substr(2, 9) +
                    Math.random().toString(36).substr(2, 9) +
                    Math.random().toString(36).substr(2, 9)).toUpperCase();

                database.collection('authentication').insertOne({
                    'u': d.u,
                    'p': d.p,
                    'token': token,
                    'aid': aid
                });

                database.collection('settings').insertOne({
                    'token': token,
                    'pictureVisible': true,
                    'online': false
                });

                fs.mkdir(`data/aids/${aid}`, { recursive: true }, () => {
                    fs.writeFile(`data/aids/${aid}/index.html`, `
                        <html>
                            <head>
                                <title>Refering</title>
                                <meta charset="UTF-8">
                            </head>
                            <body>
                                <script>
                                    window.location = '/?_aid=${aid}'
                                </script>
                            </body>
                        </html>
                    `, () => {});
                });


                fs.mkdir(`data/users/${token}/profile-pictures`, { recursive: true }, () => {
                    fs.writeFile(`data/users/${token}/profile-pictures/1.png`, generateProfilePicture(d.u.substr(0, 1)), 'utf-8', (e) => {
                        if(!e) {
                            log(socket.request.connection.remoteAddress + chalk.yellow(' CREATED AN ACCOUNT'));
                            socket.emit('signed-up');
                        }
                    });
                });
            }
        })
    })
};

let generateProfilePicture = (l) => {
    let canvas = createCanvas(300, 300);
    let ctx = canvas.getContext('2d');

    ctx.font = '150px Arial';

    ctx.fillStyle = colors[Math.floor(Math.random() * colors.length)];
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    ctx.fillStyle = '#FFFFFF';
    ctx.textAlign = 'center';
    ctx.fillText(l.toUpperCase(), 150, 200);

    return canvas.toBuffer();
};
