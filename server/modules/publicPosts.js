module.exports = (socket, database, user) => {
    socket.on('get-public-posts', () => {
        database.collection('public-posts').find({
            by: user.token
        }).toArray((e, d) => {
            if(d.length > 0) {
                d.forEach((el) => {
                   delete el["_id"];
                });
            }

            socket.emit('get-public-posts-result', d);
        })
    });

    socket.on('new-public-post', (d) => {
        database.collection('public-posts').insertOne({
            text: d.text,
            originalText: d.originalText,
            time: new Date(),
            by: user.token,
            likes: 0
        });
    });
};