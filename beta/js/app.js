let app = (c) => {
    let resizingSidebar = false;

    if(appOptions.get('_aid') !== null) {
        modal({
            h: 'Conversation',
            t: `Are you sure you want to open a chat with the user <b>${appOptions.get('_aid')}</b>?`,
            b: '<div class="button-small close-button red">Close</div><div class="button-small open-chat">Open chat</div>'
        });

        document.getElementsByClassName('open-chat')[0]
            .addEventListener('click', () => {
                closeModal();

                getViaAid(appOptions.get('_aid'), (chatId, name, token) => {
                    openChat(chatId, name, token);
                });
        });

        document.getElementsByClassName('close-button')[0]
            .addEventListener('click', () => {
                closeModal();
        });
    }

    document.getElementsByClassName('name-options')[0].getElementsByClassName('fa-chevron-down')[0].addEventListener('click', () => {
        let cm = () => {
            document.getElementsByClassName('name-options')[0].getElementsByClassName('fa-chevron-down')[0].classList.remove('rotated');
            document.getElementsByClassName('name-options')[0].getElementsByClassName('tooltip')[0].classList.remove('visible');
        };

        if(document.getElementsByClassName('name-options')[0].getElementsByClassName('fa-chevron-down')[0].classList.contains('rotated')) {
            cm();
        } else {
            document.getElementsByClassName('name-options')[0].getElementsByClassName('fa-chevron-down')[0].classList.add('rotated');
            document.getElementsByClassName('name-options')[0].getElementsByClassName('tooltip')[0].classList.add('visible');
        }

        document.addEventListener('click', (e) => {
            if(
                document.getElementsByClassName('name-options')[0].getElementsByClassName('fa-chevron-down')[0].classList.contains('rotated') &&

                (
                    e.target.parentElement !== document.getElementsByClassName('name-options')[0].getElementsByClassName('tooltip')[0] &&
                    e.target !== document.getElementsByClassName('name-options')[0].getElementsByClassName('tooltip')[0] &&
                    e.target !== document.getElementsByClassName('name-options')[0].getElementsByClassName('fa-chevron-down')[0]
                )
            ) {
                cm();
            }
        })
    });

    document.getElementsByClassName('logout')[0].addEventListener('click', () => {
       logout();
    });

    document.getElementsByClassName('fa-paperclip')[0].addEventListener('click', () => {
        document.getElementsByClassName('send-file')[0].click();

        document.getElementsByClassName('send-file')[0].addEventListener('change', (e) => {
            const reader = new FileReader();

            reader.addEventListener('load', (f) => {
                compressImage(f.target.result, (result) => {
                    modal({
                        t: `<img alt="" src="${result}">`,
                        b: `<div class="button-small">ABBRECHEN</div><div class="button-small red">SENDEN</div>`,
                        o: {
                            noHeader: true,
                        }
                    });

                    document
                        .getElementsByClassName('modal')[0]
                        .getElementsByClassName('button-small')[1]
                        .addEventListener('click', () => {
                            closeModal();

                            send(result, 'image/jpeg',
                                document.getElementsByClassName('preview-chat')[0].getAttribute('token'),
                                document.getElementsByClassName('preview-chat')[0].getAttribute('chatId'));
                        });

                    document
                        .getElementsByClassName('modal')[0]
                        .getElementsByClassName('button-small')[0]
                        .addEventListener('click', () => {
                            closeModal();
                        });
                });
            });

            reader.readAsDataURL(e.target.files[0]);
        });
    });

    setInterval(() => {
        if(document.getElementsByClassName('new-message-text')[0].value.length > 0 &&
            document.getElementsByClassName('send-button')[0].classList.contains("record") &&
            (document.getElementsByClassName('new-message-text')[0].value.toString().length === document.getElementsByClassName('new-message-text')[0].value.toString().trim().length)
        ) {
            document.getElementsByClassName('send-button')[0].classList.remove("record")
        } else if(document.getElementsByClassName('new-message-text')[0].value.length === 0 &&
            !document.getElementsByClassName('send-button')[0].classList.contains("record")) {
            document.getElementsByClassName('send-button')[0].classList.add("record")
        }
    }, 500);

    document.getElementsByClassName('send-button')[0].addEventListener('mousedown', () => {
        if(!document.getElementsByClassName('send-button')[0].classList.contains("record")) {
            send(document.getElementsByClassName('new-message-text')[0].value, 'text/plain',
                document.getElementsByClassName('preview-chat')[0].getAttribute('token'),
                document.getElementsByClassName('preview-chat')[0].getAttribute('chatId'));
        } else {
            let r = record((d) => {
                modal({
                    h: ``,
                    t: `<audio class="voice-message-preview" src="#" controls>`,
                    b: `
                        <div class="button-small">ABBRECHEN</div>
                        <div class="button-small red">SENDEN</div>
                    `
                });

                document.getElementsByClassName('voice-message-preview')[0].src = d.data;
                document.getElementsByClassName('voice-message-preview')[0].autoplay = true;

                [...document.getElementsByClassName('modal')[0].getElementsByClassName('button-small')].forEach((b) => {
                    b.addEventListener('click', () => {
                        if (b.classList.contains('red')) {
                            send(d.data, 'audio/webm',
                                document.getElementsByClassName('preview-chat')[0].getAttribute('token'),
                                document.getElementsByClassName('preview-chat')[0].getAttribute('chatId'), { duration: d.duration });

                            closeModal();
                        } else {
                            closeModal();
                        }
                    })
                });
            });

            document.getElementsByClassName('send-button')[0].addEventListener('mouseup', () => {
                r.stop();
            });
        }
    });

    document.addEventListener('keydown', (e) => {
        if(e.keyCode === 13 && document.getElementsByClassName('preview-chat')[0].getAttribute('token') !== '') {
            if(!document.getElementsByClassName('send-button')[0].classList.contains("record")) {
                send(document.getElementsByClassName('new-message-text')[0].value, 'text/plain',
                    document.getElementsByClassName('preview-chat')[0].getAttribute('token'),
                    document.getElementsByClassName('preview-chat')[0].getAttribute('chatId'));
            }
        }
    });

    document.getElementsByClassName('resizer')[0].addEventListener('mousedown', () => {
        resizingSidebar = true;
    });

    document.addEventListener('mouseup', () => {
        if(resizingSidebar) {
            resizingSidebar = false;
        }
    });

    document.addEventListener('mousemove', (e) => {
        if(resizingSidebar) {
            let l = Math.round(100 * (e.clientX / window.innerWidth));

            l = l < 1 ? 1 : l;

            document.getElementsByClassName('app')[0].style
                .setProperty('grid-template-columns', `${l}% ${100 - l}%`);
        }
    });

    c();
};