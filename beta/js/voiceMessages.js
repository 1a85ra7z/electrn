let record = (c) => {
    let mr;
    let ac = [];

    navigator.mediaDevices.getUserMedia({ audio: true })
        .then((s) => {
            mr = new MediaRecorder(s);

            mr.start();

            mr.addEventListener("dataavailable", event => {
                ac.push(event.data);
            });
        });

    return {
        stop: () => {
            mr.addEventListener("stop", () => {
                let a = new Audio(URL.createObjectURL(new Blob(ac, {type: 'audio/webm'})));
                a.addEventListener('loadeddata', () => {
                    let r = new FileReader();
                    r.readAsDataURL(new Blob(ac, {type: 'audio/webm'}));
                    r.addEventListener('loadend', () => {
                        c({
                            data: r.result,
                            duration: a.duration
                        })
                    });
                });
            });

            mr.stop();
        }
    }
};
