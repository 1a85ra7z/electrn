window.addEventListener('load', () => {
    let bugMode = false;

   if(bugMode) {
       document.getElementsByClassName('bug')[0].addEventListener('click', () => {
           modal({
               h: `Report a bug`,
               t: `<div class="input bug-input">
                    <input class="name-input report-log" type="text" spellcheck="false"/>
                </div>`,
               b: `
            <div class="button-small">CANCEL</div>
            <div class="button-small red">SEND</div>
        `
           });

           loadInputs();
           document.getElementsByClassName('report-log')[0].focus();

           document.getElementsByClassName('bug')[0].setAttribute('style', 'opacity: 0;');

           [...document.getElementsByClassName('modal')[0]
               .getElementsByClassName('button-small')].forEach((b) => {

               b.addEventListener('click', () => {
                   if (!b.classList.contains('red')) {
                       closeModal();
                   } else {
                       socket.emit('bug', {
                           m: document.getElementsByClassName('modal')[0]
                               .getElementsByClassName('report-log')[0].value
                       });

                       closeModal();
                   }

                   document.getElementsByClassName('bug')[0].setAttribute('style', '');
               });
           });
       });
   } else {
       document.getElementsByClassName('bug')[0].style.setProperty('display', 'none');
   }
});