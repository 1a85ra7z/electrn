let settings = (user, c) => {
    let sidebar = document.getElementsByClassName('sidebar')[0]
        .getElementsByClassName('profile')[0];

    sidebar.getElementsByClassName('name-string')[0].innerHTML = user.u;
    sidebar.getElementsByClassName('id')[0].innerHTML = user.aid;

    getProfilePicture(user, (d) => {
        sidebar
            .getElementsByClassName('profile-picture')[0]
            .setAttribute('src', d);

        c();
    });

    sidebar.getElementsByClassName('id')[0].addEventListener('click', () => {
        modal({
            h: 'Share user ID',
            t: `Click on the icon below to copy the link to your account.
                <br><br>
                <div class="copy-field">
                    <i class="fas fa-clipboard"></i>
                    <div class="input-default">
                        <input type="text" class="copy-body" spellcheck="false" value="${'http://' + window.location.hostname + '/user/' + sidebar.getElementsByClassName('id')[0].innerHTML}"/>
                    </div>
                </div>`,
            b: '<div class="button-small">Close</div>'
        });

        document.getElementsByClassName('copy-body')[0].select();

        document.getElementsByClassName('button-small')[0].addEventListener('click', () => {
           closeModal();
        });

        document.getElementsByClassName('fa-clipboard')[0].addEventListener('click', function() {
            closeModal();
        });
    })
};