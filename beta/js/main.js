let appOptions = new URLSearchParams(location.search);
//window.history.pushState('#', 'ELECTRN Messenger', '/#');

window.addEventListener('load', () => {
    setup(() => {
        auth((user) => {
            load('pages/_app.html', () => {
                document.getElementsByClassName('preload')[0].classList.remove("transitions");
                document.getElementsByClassName('preload')[0].classList.remove("invisible");

                setupApp(user);
            }, true);
        });
    });
});

let setupApp = (user) => {
    let loadingTime = performance.now();

    app(() => {
        search();
        newChat(user);

        settings(user, () => {
            chatPreview(user);

            chats(user, () => {
                setTimeout(() => {
                    document.getElementsByClassName('preload')[0].classList.add("transitions");
                    document.getElementsByClassName('preload')[0].classList.add("invisible");
                }, 3500 - (performance.now() - loadingTime))
            })
        });
    });
};