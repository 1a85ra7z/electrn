let activePage = "";
let activeConnection = false;
let socket;
let intervals = [];

// SETTING UP SOCKET CONNECTION
let setup = (c) => {
    log('LOADED DATA', '#ff0051');

    let s = () => {
        if(!activeConnection) {
            let closedModal = false;
            socket = io('http://127.0.0.1:8810');

            socket.on('connect', () => {
                log('CONNECTED', '#2ecc71');
                activeConnection = true;
                clearInterval(i);
                c();
            });

            setTimeout(() => {
                if(!activeConnection && !closedModal) {
                    modal({
                        h: 'Connection',
                        t: "We can't establish a connection to our servers. Please check your internet connection.",
                        b: "",
                        o: {
                            closable: false,
                            noButton: true
                        }
                    });
                }
            }, 1000);

            let i = setInterval(() => {
                if(!activeConnection && activePage !== 'pages/__error--socket-connection.html' && closedModal) {
                    load('pages/__error--socket-connection.html', () => {});
                }
            }, 500);
        }
    };

    if(document.cookie === "") {
        modal({
            h: "Cookies",
            t: "Um unsere Webseite für Sie optimal zu gestalten und fortlaufend verbessern zu können, verwenden wir Cookies. Durch die weitere Nutzung der Webseite stimmen Sie der Verwendung von Cookies zu. Zur <a href='privacy/'>Datenschutzbestimmung</a>.",
            b: "<a href='#' class='button-small'>Ich akzeptiere</a>",
        });

        document
            .getElementsByClassName('modal')[0]
            .getElementsByClassName('button-small')[0]
            .addEventListener('click', () => { document.cookie = "allowed-cookies=true;"; s()});
    } else {
        s();
    }
};