let encodeHTML = (t) => {
    let m = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return t.replace(/[&<>"']/g, (r) => { return m[r]; });
};

let loadInputs = () => {
    [...document.getElementsByClassName("input")].forEach((el, i) => {
        let or = el;
        el = el.getElementsByTagName("input")[0];

        el.addEventListener('focus', () => {
            document.getElementsByClassName("input")[i].classList.add("focus");
        });

        el.addEventListener('focusout', () => {
            document.getElementsByClassName("input")[i].classList.remove("focus");
        });

        setInterval(() => {
            if(el.value !== "") {
                if(!or.classList.contains("filled")) {
                    or.classList.add("filled");
                }
            } else {
                if(or.classList.contains("filled")) {
                    or.classList.remove("filled");
                }
            }
        });

        if(i === 0) {
            el.focus();
        }
    });
};

let load = (p, h, reset) => {
    let r = new XMLHttpRequest();

    r.addEventListener('readystatechange', () => {
        if(r.status === 200 && r.readyState === 4) {
            document.body.getElementsByClassName('render')[0].innerHTML = r.responseText;

            activePage = p;

            loadInputs();
            closeModal();


            if(reset) {
                let i = window.setInterval(() => {},100000);

                while(i >= 0) {
                    window.clearInterval(i--);
                }
            }

            h();
        }
    });

    r.open("GET", p);
    r.send();
};

let closeModal = () => {
    if(document.getElementsByClassName('modal')[0].classList.contains('visible')) {
        document.getElementsByClassName('modal')[0].classList.remove('visible');
    }

    if(document.getElementsByClassName('curtain')[0].classList.contains('visible')) {
        document.getElementsByClassName('curtain')[0].classList.remove('visible');
    }
};

let modal = (o) => {
    let closeable = true;
    let m = document.getElementsByClassName('modal')[0];

    document.getElementsByClassName('curtain')[0].removeEventListener('click', () => {});

    document.getElementsByClassName('curtain')[0].addEventListener('click', () => {
        if(closeable) {
            closeModal();
        }
    });

    let r = {
        close: () => {
            closeModal();
        }
    };

    if(m.getElementsByClassName('buttons')[0].classList.contains('hidden')) {
        m.getElementsByClassName('buttons')[0].classList.remove('hidden')
    }

    if(m.getElementsByClassName('header')[0].classList.contains('hidden')) {
        m.getElementsByClassName('header')[0].classList.remove('hidden')
    }

    if(m.getElementsByClassName('text')[0].classList.contains('hidden')) {
        m.getElementsByClassName('text')[0].classList.remove('hidden')
    }

    m.getElementsByClassName('header')[0].innerHTML = o.h;
    m.getElementsByClassName('text')[0].innerHTML = o.t;
    m.getElementsByClassName('buttons')[0].innerHTML = o.b;

    m.classList.add('visible');

    if(typeof o.o === 'object') {
        if(typeof o.o.noHeader === 'boolean') {
            if(o.o.noHeader) {
                if(!m.getElementsByClassName('header')[0].classList.contains('hidden')) {
                    m.getElementsByClassName('header')[0].classList.add('hidden');
                }
            }
        }

        if(typeof o.o.noText === 'boolean') {
            if(o.o.noText) {
                if(!m.getElementsByClassName('text')[0].classList.contains('hidden')) {
                    m.getElementsByClassName('text')[0].classList.add('hidden');
                }
            }
        }

        if(typeof o.o.closable === 'boolean') {
            if(!o.o.closeable) {
                closeable = false;
            }
        }
    }

    document.getElementsByClassName('curtain')[0].classList.add('visible');

    return r;
};
