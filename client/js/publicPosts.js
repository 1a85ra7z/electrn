let getPublicPosts = (c) => {
    socket.emit('get-public-posts');

    socket.once('get-public-posts-result', (d) => {
        if (d !== "") {
            c(d);
        } else {
            c(null)
        }
    });
};