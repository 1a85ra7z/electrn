let getUserPicture = (t, c) => {
    socket.emit("get-aids-picture", t);

    socket.once(`user-picture-${t}`, (d) => {
        c(d);
    });
};

let getProfilePicture = (user, callback) => {
    getUserPicture(user.token, (d) => {
        callback(d);
    });
};

let compressImage = (file, c) => {
    const img = new Image();
    img.src = file;

    img.onload = () => {
        let x = 300 / img.naturalWidth;
        const e = document.createElement("canvas");

        e.width = img.naturalWidth * x;
        e.height = img.naturalHeight * x;

        const ctx = e.getContext('2d');

        ctx.drawImage(img, 0, 0, e.width, e.height);

        c(e.toDataURL());
    };
};
