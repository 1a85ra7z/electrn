let log = (message, color) => {
    console.log(`%c ${message} `,
        `font-weight:bold;font-familiy:sans-serif;background:${color};font-size:1.5em;color:white;border-radius:5px;padding:5px 0;`);
};