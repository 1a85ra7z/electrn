let search = () => {
    let s = document.getElementsByClassName('search-input')[0];
    let c = document.getElementsByClassName('chats')[0];
    let ov = '';

    setInterval(() => {
        if(ov !== s.value) {
            if(s.value.replace(/[^a-zA-Z]/g, '').toLowerCase().length > 0) {
                [...c.children].forEach((ch) => {
                    if(
                        !ch.getElementsByClassName('name')[0].innerHTML.toString().replace(/[^a-zA-Z]/g, '').toLowerCase().includes(s.value.replace(/[^a-zA-Z]/g, '').toLowerCase()) &&
                        !ch.getElementsByClassName('last-message')[0].innerHTML.toString().replace(/[^a-zA-Z]/g, '').toLowerCase().includes(s.value.replace(/[^a-zA-Z]/g, '').toLowerCase()) &&
                        !ch.getElementsByClassName('time')[0].innerHTML.toString().replace(/[^a-zA-Z]/g, '').toLowerCase().includes(s.value.replace(/[^a-zA-Z]/g, '').toLowerCase())
                    ) {
                        ch.setAttribute('style', 'display: none;');
                    } else {
                        ch.setAttribute('style', '');
                    }
                });
            } else {
                [...c.children].forEach((ch) => {
                    ch.setAttribute('style', '');
                });
            }

            ov = s.value;
        }
    }, 50);
};
