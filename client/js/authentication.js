let deleteAuthCode = () => {
    document.cookie = "electrn-auth-code=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
};

let logout = () => {
    deleteAuthCode();
    location.reload();
};

auth = (callback) => {
    let loggedIn = false;
    let nameInUse = false;

    let newAuthCode = () => {
        socket.emit("create-login-auth-code");

        socket.once("new-auth-code", (c) => {
            document.cookie = "electrn-auth-code=" + c;
        });
    };

    socket.on("name-in-use", () => {
        nameInUse = true;
    });

    socket.on("name-not-in-use", () => {
        nameInUse = false;
    });

    let defaultLogin = () => {
        load("pages/__authentication.html", () => {
            let s = "";

            document.getElementsByClassName("help")[0].addEventListener('click', () => {
                modal({
                    h: "Authentifizierung",
                    t: "Um ein Profil zu erstellen, gebe Deine Daten ein und klicke auf 'Anmelden'.\n Für eine erneute Anmeldung müssen nur die Daten eingegeben werden.",
                    b: "<a href='#' class='button-small'>SCHLIEßEN</a>"
                });

                document
                    .getElementsByClassName('modal')[0]
                    .getElementsByClassName('button-small')[0]
                    .addEventListener('click', () => { closeModal() });
            });

            intervals.push(setInterval(() => {
                if(
                    document.getElementsByClassName("name-input")[0].value.length >= 3 &&
                    document.getElementsByClassName("password-input")[0].value.length >= 8 && !nameInUse) {
                    if(document.getElementsByClassName("create")[0].classList.contains("disabled")) {
                        document.getElementsByClassName("create")[0].classList.remove("disabled")
                    }
                } else if(!document.getElementsByClassName("create")[0].classList.contains("disabled")) {
                    document.getElementsByClassName("create")[0].classList.add("disabled")
                }

                if(document.getElementsByClassName("name-input")[0].value.length >= 3) {
                    if(document.getElementsByClassName("name")[0].getElementsByClassName('tooltip')[0].classList.contains("visible")) {
                        document.getElementsByClassName("name")[0].getElementsByClassName('tooltip')[0].classList.remove("visible")
                    }
                } else if(document.getElementsByClassName("name-input")[0] === document.activeElement) {
                    if(!document.getElementsByClassName("name")[0].getElementsByClassName('tooltip')[0].classList.contains("visible")) {
                        document.getElementsByClassName("name")[0].getElementsByClassName('tooltip')[0].classList.add("visible")
                    }
                }

                if(document.getElementsByClassName("password-input")[0].value.length >= 8) {
                    if(document.getElementsByClassName("password")[0].getElementsByClassName('tooltip')[0].classList.contains("visible")) {
                        document.getElementsByClassName("password")[0].getElementsByClassName('tooltip')[0].classList.remove("visible")
                    }
                } else if(document.getElementsByClassName("password-input")[0] === document.activeElement) {
                    if(!document.getElementsByClassName("password")[0].getElementsByClassName('tooltip')[0].classList.contains("visible")) {
                        document.getElementsByClassName("password")[0].getElementsByClassName('tooltip')[0].classList.add("visible")
                    }
                }

                document.getElementsByClassName("name-input")[0].value =
                    document.getElementsByClassName("name-input")[0].value.replace(/[^a-zA-Z0-9_ ]/g, '').trimLeft();
                document.getElementsByClassName("password-input")[0].value =
                    document.getElementsByClassName("password-input")[0].value.replace(/[^a-zA-Z0-9_]/g, '').trim();

                if(loggedIn) {
                    clearInterval(intervals[intervals.length]);
                } else {
                    if(
                        s !== (
                        document.getElementsByClassName("name-input")[0].value +
                        document.getElementsByClassName("password-input")[0].value) &&
                        !loggedIn &&
                        document.getElementsByClassName("name-input")[0].value !== ""
                        && document.getElementsByClassName("password-input")[0].value !== ""

                    ) {
                        s = document.getElementsByClassName("name-input")[0].value +
                            document.getElementsByClassName("password-input")[0].value;

                        let data = {
                            u: document.getElementsByClassName("name-input")[0].value.trim(),
                            p: sha256(document.getElementsByClassName("password-input")[0].value)
                        };

                        socket.emit("check-aids-data", data);
                    }
                }
            }, 300));

            document.getElementsByClassName("create")[0].addEventListener('click', () => {
                let data = {
                    u: document.getElementsByClassName("name-input")[0].value.trim(),
                    p: sha256(document.getElementsByClassName("password-input")[0].value)
                };

                socket.emit("sign-up", data);

                socket.once('signed-up', () => {
                    socket.emit("check-aids-data", data);
                })
            });
        });
    };

    socket.on('disconnect', () => {
        log('DISCONNECTED', '#ee5253');
        load('pages/__error--socket-connection.html', () => {}, true);
        activeConnection = false;
    });

    socket.on('logged-in', (user) => {
        if(activePage === "pages/__authentication.html") {
            if(document.getElementsByClassName('stay-signed-in')[0].value === "on" && document.cookie === "") {
                newAuthCode();
            }
        }

        if(document.cookie !== "") {
            newAuthCode();
        }

        loggedIn = true;
        log('LOGGED IN', '#2c3e50');

        callback(user);
    });

    if(document.cookie.split("electrn-auth-code=")[1] !== "") {
        socket.emit("check-auth-code", document.cookie.split("electrn-auth-code=")[1]);

        socket.on("failed-log-in", () => {
            deleteAuthCode();
            defaultLogin();
        });
    } else {
        defaultLogin();
    }
};