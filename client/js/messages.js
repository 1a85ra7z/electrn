let messages = (user) => {
    let chat = document.getElementsByClassName('preview-chat')[0];
    let currentMessages;

    setInterval(() => {
        if(chat.getAttribute('token') !== "") {
            getMessages(chat.getAttribute('token'), (messages) => {
                if(JSON.stringify(messages) !== JSON.stringify(currentMessages) && messages.length !== 0) {
                currentMessages = messages;
                chat.getElementsByClassName('messages-chat')[0].innerHTML = "";
                    [...messages].forEach((message, index) => {
                        chat.getElementsByClassName('messages-chat')[0].innerHTML += `
                            <div class="message ${message.sentBy === chat.getAttribute('token') ? 'left' : 'right'}">
                                <div class="message-content">
                                    <span>${message.message}</span>
                                    <i class="fas fa-ellipsis-v"></i>
                                </div>
                            </div>
                        `;

                        setTimeout(() => {
                            chat.getElementsByClassName('messages-chat')[0]
                                .children[index]
                                .getElementsByClassName('fa-ellipsis-v')[0].addEventListener('click', () => {

                                modal({
                                    h: `„${message.message}“`,
                                    t: `<i class="fas fa-clock"></i> ${new Date(message.time).getHours()}:${new Date(message.time).getMinutes()}
                                        <br><br>
                                        <i class="fas fa-eye"></i> ${message.status === 2 ? ' true' : '-'}
                                        <br><br>
                                        <a href="#" class="deleteMessage"><i class="fas fa-trash"></i> Delete</a>`,
                                    b: 'Close'
                                });

                                document.getElementsByClassName('deleteMessage')[0].addEventListener('click', () => {
                                    modal({
                                        h: 'Delete Message',
                                        t: `<input type='checkbox' class="deleteForOtherPerson"> Delete also for <h>${document.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].innerHTML}</h>`,
                                        b: 'Delete',

                                        click: function() {
                                            let users = [
                                                user.token
                                            ];

                                            if(document.getElementsByClassName('deleteForOtherPerson')[0].checked === true) {
                                                users = message.u;
                                            }

                                            socket.emit('delete-message', {
                                                id: message.id,
                                                users: users
                                            });
                                        }
                                    })
                                })
                            })
                        })
                    });

                    if (messages[messages.length - 1].status === 2 && messages[messages.length - 1].sentBy !== chat.getAttribute('token')) {
                        chat.getElementsByClassName('messages-chat')[0].innerHTML += `
                                <i class="far fa-eye"></i>
                        `;
                    }

                    document.getElementsByClassName('messages-chat')[0].scrollTop = document.getElementsByClassName('messages-chat')[0].scrollHeight;
                } else if(messages.length === 0 && chat.getElementsByClassName('no-messages').length === 0) {
                    chat.getElementsByClassName('messages-chat')[0].innerHTML = "";

                    chat.getElementsByClassName('messages-chat')[0].innerHTML += `
                            <div class="no-messages">
                                <video src="assets/img/no-messages.mp4" autoplay loop></video>
                                <h1>Gäähnende Leere hier...</h1>
                                <p>Sei der Erste und schreibe <h>
                                    ${document.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].innerHTML}
                                 </h> eine Nachricht!</p>
                            </div>
                        `;
                }
            });
        }
    }, 500);

    document.getElementsByClassName('return-button')[0].addEventListener('click', () => {
        document.getElementsByClassName('sidebar')[0].classList.remove('invisible');
        document.getElementsByClassName('preview-chat')[0].setAttribute('chatId', "");
        document.getElementsByClassName('preview-chat')[0].setAttribute('token', "");
        currentMessages = [];
    });
};

let getMessages = (token, c) => {
    socket.emit('get-messages', token);

    socket.once('get-messages-result', (d) => {
        if (d !== "") {
            c(d);
        } else {
            c(null)
        }
    });
};