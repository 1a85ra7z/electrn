let openedChat = "";
let oldMessageCount = 0;
let unreadMessageCount = 0;

let originalTitle = document.title;

let closeChat = () => {
    document.getElementsByClassName('sidebar')[0].classList.remove('invisible');
    document.getElementsByClassName('preview-chat')[0].setAttribute('chatId', "");
    document.getElementsByClassName('preview-chat')[0].setAttribute('token', "");

    [...document.getElementsByClassName('chats')[0].children].forEach((el) => {
        if(el.classList.contains('opened')) {
            el.classList.remove('opened');
            openedChat = "";
        }
    });
};

let getChats = (c) => {
    socket.emit("get-chats");

    socket.once("get-chats-result", (d) => {
        if (d !== "") {
            c(d);
        } else {
            c(null);
        }
    });
};

let openChat = (chatId, name, token) => {
    let chat = document.getElementsByClassName('preview-chat')[0];

    getUserPicture(token, (d) => {
        chat.getElementsByClassName('header-chat')[0]
            .getElementsByClassName('chat-profile-picture')[0]
            .setAttribute('src', d);
    });

    openedChat = token;

    if(chatId !== chat.getAttribute('chatId')) {
        if (chat.getElementsByClassName('header-chat')[0].classList.contains('hidden')) {
            chat.getElementsByClassName('header-chat')[0].classList.remove('hidden');
        }

        if (chat.getElementsByClassName('new-message')[0].classList.contains('hidden')) {
            chat.getElementsByClassName('new-message')[0].classList.remove('hidden');
        }

        chat.getElementsByClassName('messages-chat')[0].innerHTML = "";

        chat.setAttribute('chatId', chatId);
        chat.setAttribute('token', token);

        chat.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].innerHTML = name;

        document.getElementsByClassName('messages-chat')[0].scrollTop = document.getElementsByClassName('messages-chat')[0].scrollHeight - document.getElementsByClassName('messages-chat')[0].clientHeight

        chat.getElementsByClassName('new-message')[0].getElementsByTagName('input')[0].focus();

        if(window.innerWidth <= 600) {
            document.getElementsByClassName('sidebar')[0].classList.add('invisible');
        }
    }
};

let chats = (user, c) => {
    let firstChatLoaded = false;
    let currentChats = [];

    setInterval(() => {
        getChats((chats) => {
            if(
                JSON.stringify(chats).replace(JSON.stringify(currentChats), '').length > 0
            ) {
                reloadChats(JSON.parse(JSON.stringify(chats)), user);
                currentChats = chats;
            }

            if(!firstChatLoaded) {
                firstChatLoaded = true;
                c();
            }
        });
    }, 500);
};

let reloadChats = (chats, user) => {
    unreadMessageCount = 0;
    document.getElementsByClassName('chats')[0].innerHTML = "";

    let chatString = [];

    [...chats].forEach((chat) => {
        let userName;
        let userToken;

        if(chat.clear[0] === user.u) {
            userName = chat.clear[1];
            userToken = chat.u[1];
        } else {
            userName = chat.clear[0];
            userToken = chat.u[0];
        }

        let time = "";

        let months = [
            'Jan', 'Feb', "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"
        ];

        if(chat.lastMessage.time !== null) {
            time = new Date(chat.lastMessage.time).getDate().toString().padStart(2, '0') + ". " + months[new Date(chat.lastMessage.time).getMonth()];
        }

        chatString.push({s: `
            <div token=${userToken} chatId=${chat.chatId} class="chat ${openedChat === userToken && openedChat ? 'opened' : ''} ${chat.unread > 0 && chat.lastMessage.sentBy !== user.token ? 'unread' : ''}">
                <img src="#" alt="#"" class="profile-picture">
                <div class="message">
                    <span class="name">${userName}</span>
                    <span class="last-message"></span>
                </div>
                <div class="meta">
                    <span class="time">${time}</span>
                    ${chat.unread > 0 && chat.lastMessage.sentBy !== user.token ? `<span class="unread-messages">${chat.unread}</span>` : ''}
                </div>
            </div>
        `, unread: chat.unread > 0, token: userToken, h: [chat.chatId, userName, userToken], lastMessage: chat.lastMessage});

        if(chat.lastMessage.sentBy !== user.token) {
            unreadMessageCount += chat.unread;
        }
    });

    chatString.sort((a, b) => {
        if(a.unread === b.unread) {
            return new Date(b.lastMessage.time) - new Date(a.lastMessage.time);
        }

        return a.unread ? -1 : 1;
    });

    chatString.forEach((c) => {
        document.getElementsByClassName('chats')[0].innerHTML += c.s;
    });

    let x = 0;

    [...document.getElementsByClassName('chats')[0].children].forEach((e, i) => {
        i -= x;

        if(!e.classList.contains("ignore")) {
            let maxChars = Math.floor(e.getElementsByClassName("last-message")[0].clientWidth / parseFloat(getComputedStyle(e.getElementsByClassName("last-message")[0]).fontSize));

            if(chatString[i].lastMessage.text.length - 3 > maxChars) {
                chatString[i].lastMessage.text = chatString[i].lastMessage.text.substr(0, maxChars - 3) + "...";
            }

            switch(chatString[i].lastMessage.type) {
                case 'text/plain':
                    e.getElementsByClassName("last-message")[0].innerHTML = chatString[i].lastMessage.text;
                    break;
                case 'audio/webm':
                    e.getElementsByClassName("last-message")[0].innerHTML = '<i class="fas fa-microphone"></i>';
                    break;
                case 'image/jpeg':
                case 'image/png':
                    e.getElementsByClassName("last-message")[0].innerHTML = '<i class="fas fa-image"></i>';
                    break;
                default:
                    e.getElementsByClassName("last-message")[0].innerHTML = '';
            }

            getUserPicture(chatString[i].token, (d) => {
                e.getElementsByClassName("profile-picture")[0].setAttribute("src", d);
            });

            e.addEventListener('click', () => {
                document.getElementsByClassName('search-input')[0].value = '';

                [...document.getElementsByClassName('chats')[0].children].forEach((ch) => {
                    ch.setAttribute('style', '');
                });

                [...document.getElementsByClassName('chats')[0].children].forEach((el) => {
                    if(el.classList.contains('opened')) {
                        el.classList.remove('opened');
                    }
                });

                e.classList.add("opened");

                openChat(chatString[i].h[0], chatString[i].h[1], chatString[i].h[2])
            });
        }
    });

    if(oldMessageCount < unreadMessageCount) {
        if(oldMessageCount !== 0) {
            document.title = `(${unreadMessageCount}) ${originalTitle}`;
            let s = new Audio('./assets/audio/new-message.wav');
            s.play();
        } else {
            document.title = `(${unreadMessageCount}) ${originalTitle}`;
        }

        oldMessageCount = unreadMessageCount;
    } else if(unreadMessageCount === 0) {
        document.title = `${originalTitle}`;
    }
};