let messages = [];
let checkedMessages = [];

let count = 19;

let currentToken = '';
let icons = [];

let playingVoiceMessage = [];

let currentIndex = -1;
let messageCount = 0;

let currentMessages = [];
let deletedMessages = [];

Array.prototype.diff = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
};

let reloadMessages = (user) => {
    let chat = document.getElementsByClassName('preview-chat')[0];

    getMessages(chat.getAttribute('token'), count,(m, i, l) => {
        if(messageCount < l && currentIndex !== i) {
            if(l - i === l) {
                chat.getElementsByClassName('messages-chat')[0].append(messageToString(m, i, user));
                currentMessages.push(m);
            } else {
                chat.getElementsByClassName('messages-chat')[0].prepend(messageToString(m, i, user));
                currentMessages.unshift(m);
            }

            messageCount++;
        } else if(messageCount > l) {
            deletedMessages.push(m);

            if(i + 1 === l) {
                for(let y = 0; y < currentMessages.length - deletedMessages.length + 1; y++) {
                    let found = false;

                    [...currentMessages, ...deletedMessages].forEach((cm) => {
                        if(!found) {
                            let c = 0;

                            [...currentMessages, ...deletedMessages].forEach((cmm) => {
                                if(cm.id === cmm.id) {
                                    c++;
                                }
                            });

                            if(c === 1) {
                                currentMessages.forEach((ccm, index) => {
                                    if(ccm.id === cm.id) {
                                        chat.getElementsByClassName('messages-chat')[0].children[index].remove();
                                        currentMessages.splice(index, 1);
                                        found = true;
                                    }
                                });
                            }
                        }
                    });
                }
            }
        }

        if(i + 1 === count) {
            count += count;
        }

        currentIndex = i;
    });
};

let chatPreview = (user) => {
    let chat = document.getElementsByClassName('preview-chat')[0];

    icons.push(chat.getElementsByClassName('header-chat')[0]
        .getElementsByClassName('fa-share')[0]);

    icons.push(chat.getElementsByClassName('header-chat')[0]
        .getElementsByClassName('fa-feather-alt')[0]);

    icons.push(chat.getElementsByClassName('header-chat')[0]
        .getElementsByClassName('fa-trash')[0]);

    icons.push(chat.getElementsByClassName('header-chat')[0]
        .getElementsByClassName('fa-info-circle')[0]);

    icons[3].addEventListener('click', () => {
        if(checkedMessages.length === 1) {
            checkedMessages[0].info();
        }
    });

    icons[2].addEventListener('click', () => {
         deleteMessages(checkedMessages, user);
    });

    icons[0].addEventListener('click', () => {
        resend(checkedMessages, user);
    });

    setInterval(() => {
        if(chat.getAttribute('token') !== "") {
            if(chat.getAttribute('token') !== "-1") {
                if(chat.getAttribute('token') !== currentToken) {
                    if(messages.length !== 0) {
                        messages = [];
                    }

                    currentToken = chat.getAttribute('token');
                }

                checkedMessages = [];

                socket.emit('check-aids-online-state', chat.getAttribute('token'));

                socket.on('check-aids-online-state-result', (d) => {
                    if(chat.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].classList.contains('online') !== d) {
                        if(chat.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].classList.contains('online')) {
                            chat.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].classList.remove('online');
                            chat.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].classList.add('offline')
                        } else {
                            chat.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].classList.add('online');
                            chat.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].classList.remove('offline')
                        }
                    }
                });

                reloadMessages(user);

                if(chat.classList.contains('select') && chat.getElementsByClassName('messages-chat')[0].children.length > 0) {
                    [...chat.getElementsByClassName('messages-chat')[0].children].forEach((message) => {
                        if(message.getElementsByTagName('input')[0].checked) {
                            checkedMessages.push(message);
                        }
                    });
                }

                if(checkedMessages.length > 0) {
                    if(!icons[0].classList.contains('visible')) {
                        icons[0].classList.add('visible')
                    }

                    if(!icons[2].classList.contains('visible')) {
                        icons[2].classList.add('visible')
                    }
                } else {
                    chat.classList.remove('select');

                    if(icons[0].classList.contains('visible')) {
                        icons[0].classList.remove('visible')
                    }

                    if(icons[2].classList.contains('visible')) {
                        icons[2].classList.remove('visible')
                    }
                }

                if(checkedMessages.length === 1) {
                    if(!icons[1].classList.contains('visible')) {
                        icons[1].classList.add('visible')
                    }

                    if(!icons[3].classList.contains('visible')) {
                        icons[3].classList.add('visible')
                    }
                } else {
                    if(icons[1].classList.contains('visible')) {
                        icons[1].classList.remove('visible')
                    }

                    if(icons[3].classList.contains('visible')) {
                        icons[3].classList.remove('visible')
                    }
                }
            }
        }
    });

    document.getElementsByClassName('return-button')[0].addEventListener('click', () => {
        document.getElementsByClassName('sidebar')[0].classList.remove('invisible');
        document.getElementsByClassName('preview-chat')[0].setAttribute('chatId', "");
        document.getElementsByClassName('preview-chat')[0].setAttribute('token', "");
    });
};

let resend = (m, user) => {
    let s = 0;
    let r = document.getElementsByClassName('chats')[0].children;
    let l = document.createElement('div');

    l.classList.add('resend');

    [...r].forEach((f) => {
        let m = f.cloneNode(true);

        m.getElementsByClassName('last-message')[0].remove();
        m.getElementsByClassName('meta')[0].remove();

        m.addEventListener('click', () => {
           if(m.classList.contains('selected')) {
               m.classList.remove('selected');
               s--;
           } else {
               m.classList.add('selected');

               s++;
           }

            if(s > 0) {
                if(document.getElementsByClassName('modal')[0]
                    .getElementsByClassName('button-round')[0].classList.contains('inactive')) {
                    document.getElementsByClassName('modal')[0]
                        .getElementsByClassName('button-round')[0].classList.remove('inactive');
                }
            } else if(!document.getElementsByClassName('modal')[0]
                .getElementsByClassName('button-round')[0].classList.contains('inactive')) {
                document.getElementsByClassName('modal')[0]
                    .getElementsByClassName('button-round')[0].classList.add('inactive');
            }
        });

        if(!m.classList.contains('opened')) {
            l.appendChild(m);
        }
    });


    modal({
        h: 'Resend Messages',
        t: '',
        b: '<div class="button-round primary inactive"><i class="fas fa-paper-plane"></i></div>'
    });

    document.getElementsByClassName('modal')[0]
        .getElementsByClassName('text')[0].appendChild(l);

    document.getElementsByClassName('modal')[0]
        .getElementsByClassName('button-round')[0].addEventListener('click', () => {

        if(!document.getElementsByClassName('modal')[0]
            .getElementsByClassName('button-round')[0].classList.contains('inactive')) {

            [...document.getElementsByClassName('modal')[0]
                .getElementsByClassName('text')[0]
                .getElementsByClassName('resend')[0].children].forEach((c) => {
                    if(c.classList.contains('selected')) {
                        m.forEach((message) => {
                            let type = 'text/plain';

                            switch(message.classList[2]) {
                                case 'text':
                                    type = 'text/plain';
                                    break;
                                case 'audio':
                                    type = 'audio/webm';
                                    break;
                                case 'image':
                                    type = 'image/jpeg';
                                    break;
                            }
                            send(message.getElementsByClassName('message-content')[0].getElementsByTagName('span')[0].innerHTML, type, c.getAttribute('token'), c.getAttribute('chatId'), {})
                        })
                    }
            })
        }

        closeModal();
    });
};

let getMessages = (token, count, c) => {
    let chat = document.getElementsByClassName('preview-chat')[0];

    socket.emit('get-messages', {
        count: count,
        token: token
    });

    socket.once(`get-messages-result-${token}`, (length) => {
        if(length > 0) {
            if(chat.getElementsByClassName('no-messages').length !== 0) {
                chat.getElementsByClassName('messages-chat')[0].innerHTML = '';
            }

            [...new Array(length)].forEach((e, index) => {
                socket.once(`get-messages-result-${token}-${index}`, (d) => {
                    if (d !== null) {
                        c(d, index, length);
                    }
                });
            });
        } else if(chat.getElementsByClassName('no-messages').length === 0) {
            messages = [];
            chat.getElementsByClassName('messages-chat')[0].innerHTML = '';

            chat.getElementsByClassName('messages-chat')[0].innerHTML += `
                <div class="no-messages">
                    <video src="assets/img/no-messages.mp4" autoplay loop></video>
                    <h1>Gäähnende Leere hier...</h1>
                    <p>Sei der Erste und schreibe <h>
                        ${document.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].innerHTML}
                     </h> eine Nachricht!</p>
                </div>
            `;
        }
    });
};

let send = (text, type, token, chatId, options) => {
    if(!document.getElementsByClassName('new-message')[0].classList.contains('hidden')) {
        if(type.toLowerCase().startsWith('text/')) {
            let originalText = encodeHTML(text);

            try {
                new URL(text);

                text = `<a href=${text} target="_blank">${text}</a>`;
            } catch (_) {
                text = originalText;
            }

            document.getElementsByClassName('new-message-text')[0].value = "";
        } else if(!(type.toLowerCase().startsWith('image/') || type.toLowerCase().startsWith('audio/'))) {
            return;
        }

        socket.emit('new-message', {
            token: token,
            text: text,
            chatId: chatId,
            type: type,
            originalText: text,
            options: typeof options === 'object' ? options : null
        });
    }
};

let messageToString = (m, index) => {
    let chat = document.getElementsByClassName('preview-chat')[0];
    let message = document.createElement('div');
    let mc = document.createElement('div');
    let c = document.createElement('div');
    let ci = document.createElement('input');
    let cf = document.createElement('label');
    let cfi = document.createElement('i');
    let t;
    let d = false;
    let mm;
    let ct = 0;

    message.classList.add('message');
    message.classList.add(m.sentBy === chat.getAttribute('token') ? 'left' : 'right');
    message.classList.add(m.type.toLowerCase().split('/')[0]);

    mc.classList.add('message-content');

    c.classList.add('input-checkbox');

    ci.setAttribute('type', 'checkbox');
    ci.id = `checkbox-${index}`;

    cf.setAttribute('for', `checkbox-${index}`);

    cfi.classList.add('fas');
    cfi.classList.add('fa-check');

    cf.append(cfi);

    c.append(ci);
    c.append(cf);

    if(m.type.toLowerCase().startsWith('image/')) {
        mm = document.createElement('img');
        mm.setAttribute('src', m.text);
        mm.setAttribute('alt', '');
    } else if(m.type.toLowerCase().startsWith('audio/')) {
        mm = document.createElement('div');
        mm.classList.add('audio');

        let c = document.createElement('div');
        c.classList.add('audio-controls');

        let a = document.createElement('audio');
        a.setAttribute('src', m.text);

        let s = document.createElement('div');
        s.classList.add('audio-status');

        let t = document.createElement('div');
        t.classList.add('timer');

        let at = document.createElement('span');
        at.classList.add('audio-time');
        at.innerHTML =  `0 / <h>${Math.floor(m.options.duration).toString()}</h>`;

        t.draged = false;

        s.append(t);

        let p = document.createElement('i');

        p.classList.add('fas');
        p.classList.add('play-button');

        p.addEventListener('click', () => {
            if(!c.classList.contains('playing')) {
                if(playingVoiceMessage.length > 0) {
                    playingVoiceMessage[0].pause();
                    playingVoiceMessage[1].classList.remove('playing');
                    playingVoiceMessage = [];
                }

                c.classList.add('playing');

                if(a.currentTime > 0) {
                    playingVoiceMessage.push(a);
                    playingVoiceMessage.push(c);

                    a.play();
                } else {
                    ct = a.currentTime;

                    playingVoiceMessage.push(a);
                    playingVoiceMessage.push(c);

                    a.play();

                    let int = setInterval(() => {
                        if(a.currentTime === a.duration) {
                            playingVoiceMessage = [];
                            a.pause();

                            ct = a.currentTime = 0;
                            c.classList.remove('playing');
                            clearInterval(int);
                        } else if(ct !== a.currentTime) {
                            ct = a.currentTime;

                            at.innerHTML = `${Math.floor(ct).toString()} / <h>${Math.floor(m.options.duration).toString()}</h>`;

                            t.setAttribute('style', `left: ${Math.floor((ct / a.duration) * s.clientWidth)}px;`);
                        }
                    });
                }
            } else {
                a.pause();
                c.classList.remove('playing');
                playingVoiceMessage = [];
            }
        });


        c.append(p);
        c.append(s);
        c.append(at);

        mm.append(a);
        mm.append(c);
    } else {
        mm = document.createElement('span');
        mm.innerHTML = m.text;
    }

    mc.appendChild(mm);

    message.appendChild(mc);
    message.appendChild(c);

    mc.addEventListener('mousedown', (e) => {
        if(!message.classList.contains('audio')) {
            t = setTimeout(() => {
                d = true;

                if(!chat.classList.contains('select')) {
                    chat.classList.add('select');
                    ci.checked = true;
                } else {
                    ci.checked = !ci.checked;
                }
            }, 500);
        }
    });

    mc.addEventListener('mouseup', () => {
        if (t !== undefined) {
            clearTimeout(t);
        }

        if (!d && !chat.classList.contains('select') && !message.classList.contains('audio')) {
            message.info();
        } else if (!d && !message.classList.contains('audio')) {
            ci.checked = !ci.checked;
        }

        d = false;
    });

    message.info = () => {
        modal({
            h: `${m.type.toLowerCase().startsWith('text/') ? m.text : `<i class="fas fa-image"></i>`}`,
            t: `
                <div class="item"><i class="fas fa-clock"></i> ${new Date(m.time).getHours().toString().padStart(2, '0')}:${new Date(m.time).getMinutes().toString().padStart(2, '0')}</div>
                <div class="item"><i class="fas fa-eye"></i> ${m.status === 2 ? new Date(m.seen).getHours().toString().padStart(2, '0') + ':' + new Date(m.seen).getMinutes().toString().padStart(2, '0') : '-'}</div>
            `,
            b: '<a href=\'#\' class=\'button-small\'>SCHLIEßEN</a>'
        });

        document
            .getElementsByClassName('modal')[0]
            .getElementsByClassName('button-small')[0]
            .addEventListener('click', () => {
                closeModal()
            });
    };

    message.u = m.u;
    message.id = m.id;

    return message;
};

deleteMessages = (m, user) => {
    let chat = document.getElementsByClassName('preview-chat')[0];
    let o = true;

    m.forEach((message) => {
        if(message.u[0] !== user.token) {
            o = false;
        }
    });

    modal({
        h: `Nachricht${m.length > 1 ? `en` : ``} löschen`,
        t: `
            <div class="item">Möchtest du ${m.length > 1 ? `<h>${m.length}</h> ` : `diese`} Nachricht${m.length > 1 ? `en` : ``} löschen? </div>
        
            ${o ? `
                <div class="center item">
                    <div class="input-checkbox">
                        <input type='checkbox' id="checkbox" class="deleteForOtherPerson">
                        <label for="checkbox">Auch für <h>${
                            document.getElementsByClassName('header-chat')[0].getElementsByClassName('name')[0].innerHTML
                            }</h> löschen
                            <i class="fas fa-check"></i>
                        </label>
                    </div>
                </div>` : ``}
            `,
        b: `
            <a href="#" class="button-small">ABBRECHEN</a>
            <a class="button-small red" href="#">LÖSCHEN</a>
        `,
        o: {
            noButton: true
        }
    });

    [...document.getElementsByClassName('modal')[0].getElementsByClassName('button-small')].forEach((b) => {
        b.addEventListener('click', () => {
            if (b.classList.contains('red')) {
                let mode = 0;

                if(o) {
                    if (document.getElementsByClassName('deleteForOtherPerson')[0].checked === true) {
                        mode = 1;
                    }
                }

                m.forEach((message) => {
                    setTimeout(() => {
                        socket.emit('delete-message', {
                            id: message.id,
                            mode: mode,
                            token: chat.getAttribute('token')
                        });

                        reloadMessages(user);
                    }, 500)
                });

                closeModal();
            } else {
                closeModal();
            }
        })
    })
};