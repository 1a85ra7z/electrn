let newChat = (user) => {
    document.getElementsByClassName('new-chat')[0].addEventListener('click', () => {
        load('./pages/__new-chat.html', () => {
            let ov = '';
            let field = document.getElementsByClassName('search-input')[0];

            setInterval(() => {
                field.value = field.value.replace(/[^0-9]/g, '');

                if(ov !== field.value) {
                    if(!field.value.startsWith('@')) {
                        field.value = `@${field.value}`;
                    }

                    if(field.value.length > 9) {
                        field.value = field.value.substring(0, 9);
                    } else if(field.value.length < 9 && !document.getElementsByClassName('button')[0].classList.contains('disabled')) {
                        document.getElementsByClassName('button')[0].classList.add('disabled');
                    } else if(field.value.length === 9 && document.getElementsByClassName('button')[0].classList.contains('disabled')) {
                        document.getElementsByClassName('button')[0].classList.remove('disabled');
                    }

                    ov = field.value;
                }
            }, 50);

            document.getElementsByClassName('button')[0].addEventListener('click', () => {
                getViaAid(field.value, (chatId, name, token) => {
                    load('./pages/_app.html', () => {
                        setupApp(user);

                        setTimeout(() => {
                            openChat(chatId, name, token);
                        }, 500);
                    }, true);
                });
            });
        }, true);
    })
};
