let getViaAid = (aid, callback) => {
    socket.emit('token-via-aid', aid);

    socket.on('token-via-aid-response', (token, name) => {
        if(token !== '') {
            socket.emit('search', token);

            socket.on('search-opened', (chatId) => {
                callback(chatId, name, token);
            });
        }
    });
};